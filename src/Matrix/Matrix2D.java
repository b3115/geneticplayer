package Matrix;

import java.util.Random;

/**
 * Created by Albert Mosiałek on 17.12.2016.
 */
public class Matrix2D {
    double[][] matrix2D;
    double epsilon=0.00000001d;

    /**
     * Creates new Matrix2D from double[][].
     * Note that first dimension is length and second is height
     * @param matrix
     * @deprecated swapped dimensions might be misleading. Use new Matrix2D(length, height, value) instead
     */
    public Matrix2D(double[][] matrix){
        matrix2D=new double[matrix.length][matrix[0].length];
        for(int i=0;i<matrix.length;i++)
            for(int j=0;j<matrix[i].length;j++)
                matrix2D[i][j]=matrix[i][j];
    }

    /**
     * Creates Matrix2D filled with value.
     * @param length
     * @param height
     * @param value value to fill new Matrix2D with
     * @throws FlatMatrixException It is thrown when either height or width is 0
     */
    public Matrix2D(int length, int height, double value) throws FlatMatrixException{
        if(length==0 || height==0)
            throw new FlatMatrixException();
        double[][] m = new double[length][height];
        matrix2D=m;
        fillWith(value);
    }

    /**
     * Fills entire matrix with value
     * @param value
     */
    public void fillWith(double value){
        for(int i=0;i<matrix2D.length;i++) {
            for(int j=0;j<matrix2D[i].length;j++) {
                matrix2D[i][j]=value;
            }
        }

    }

    public int getLength(){
        return matrix2D.length; //first dimension is length
    }

    public int getHeight() {
        return matrix2D[0].length;  //second dimension is height
    }

    /**
     *
     * @param x Column number
     * @param y Row number
     * @return value from matrix[x][y]
     */
    public double getValue(int x, int y){
        return matrix2D[x][y];
    }

    /**
     *
     * @param x Column number
     * @param y Row number
     * @param value Value to be set in this cell
     */
    public void setValue(int x,int y,double value){
        matrix2D[x][y]=value;
    }

    /**
     * Fills entire matrix with random values from (0,1)
     */
    public void setRandomValues(){
        Random random=new Random();
        for(int i=0;i<getLength();i++)
            for(int j=0;j<getHeight();j++) {
                setValue(i,j,random.nextDouble());
            }
    }

    /**
     * Fills entire matrix with random values from (lowerBound, upperBound)
     */
    public void setRandomValues(double lowerBound, double upperBound){
        Random random=new Random();
        double range=upperBound-lowerBound;
        for(int i=0;i<getLength();i++)
            for(int j=0;j<getHeight();j++) {
                setValue(i,j,random.nextDouble()*range+lowerBound);
            }
    }

    /**
     * Multiplies m1 by m2
     * @param m1
     * @param m2
     * @return new matrix which is result of multiplication
     * @throws WrongMatrixMultiplyArgumentsException Thrown when m1 length is different than m2 height
     */
    public static Matrix2D multiply(Matrix2D m1, Matrix2D m2) throws WrongMatrixMultiplyArgumentsException{
        double[][] result=new double[m2.getLength()][m1.getHeight()];
        if(m1.getLength()!=m2.getHeight())
            throw new WrongMatrixMultiplyArgumentsException();
        for(int i=0;i<m1.getHeight();i++) {
            for(int j=0;j<m2.getLength();j++){
                result[j][i]=0d;
                for(int k=0;k<m1.getLength();k++) {
                   result[j][i]+=m1.getValue(k,i)*m2.getValue(j,k);
                }
            }
        }
        return new Matrix2D(result);
    }

    /**
     * Multiplies this matrix by m2
     * @param m2
     * @return new matrix which is result of multiplication
     * @throws WrongMatrixMultiplyArgumentsException Thrown when length of this matrix is different than m2 height
     */
    public Matrix2D multiplyBy (Matrix2D m2)throws WrongMatrixMultiplyArgumentsException{
        return Matrix2D.multiply(this, m2);
    }

    public void setRowValues(int rowCount, double rowValue){
        for(int i=0;i<getLength();i++){
            matrix2D[i][rowCount]=rowValue;
        }
    }

    public void setRowValues(int rowCount, double[] rowValues){
        for(int i=0;i<getLength();i++){
            matrix2D[i][rowCount]=rowValues[i];
        }
    }

    public void setRowValuesRandom(int rowCount, double lowerBound, double upperBound){
        Random random = new Random();
        for(int i=0;i<getLength();i++){
            matrix2D[i][rowCount]=random.nextDouble()*(upperBound-lowerBound)+lowerBound;
        }
    }

    public void setColumnValues(int columnCount, double columnValue){
        for(int i=0;i<getHeight();i++){
            matrix2D[columnCount][i]=columnValue;
        }
    }

    public void setColumnValues(int columnCount, double[] columnValues){
        for(int i=0;i<getHeight();i++){
            matrix2D[columnCount][i]=columnValues[i];
        }
    }

    public void setColumnValuesRandom(int columnCount, double lowerBound, double upperBound){
        Random random = new Random();
        for(int i=0;i<getHeight();i++){
            matrix2D[columnCount][i]=random.nextDouble()*(upperBound-lowerBound)+lowerBound;
        }
    }

    public double[] getRowValues(int rowCount){
        double[] d = new double[getLength()];
        for(int i=0;i<d.length;i++){
            d[i]=matrix2D[i][rowCount];
        }
        return d;
    }

    public double[] getColunValues(int columnCount){
        return matrix2D[columnCount];
    }


    /**
     *
     * @return new matrix which is COPY of this matrix
     */
    public Matrix2D clone(){
        return new Matrix2D(matrix2D);
    }

    /**
     * Checks if this matrix is the same as the other one.
     * Note that if each cell is different by less than epsilon, this function will still return true
     * @param o Matrix to be compared
     * @return true if they are equal, false if not
     */
    @Override
    public boolean equals(Object o){
        Matrix2D m = (Matrix2D)o;
        if(m.getHeight()!=getHeight() || m.getLength()!=getLength())
            return false;
        for(int i=0;i<getLength();i++)
            for(int j=0;j<getHeight();j++) {
                if(Math.abs(matrix2D[i][j]-m.getValue(i,j))>epsilon)
                    return false;
            }
        return true;
    }

    /**
     *
     * @return Entire matrix as String. Columns are separated with space and Rows with '\n'
     */
    public String toString(){
        StringBuilder s=new StringBuilder("");
        for(int j=0;j<getHeight();j++) {
            for (int i = 0; i < getLength(); i++) {
                s.append(String.valueOf(getValue(i, j))).append(' ');
            }
            s.append('\n');
        }
        return s.toString();



    }
}
