package GeneticRunner;

import Genetic.GeneticPlayerAlgorithm;
import Genetic.PlayerOrganism;
import Genetic.WholeNeuronEnhancing;
import Matrix.FlatMatrixException;
import Matrix.Matrix2D;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOfOrganisms;
        boolean isPlayerOrganismLoaded=false;
        int numberOfGenerations;
        int[] i=new int[4];
        i[0]=i[1]=i[2]=i[3]=10;
        PlayerOrganism po = new PlayerOrganism(1,i,7, 10);
        System.out.println("Usage:");
        System.out.println("learn numberOfOrganisms numberOfGenerations \\\\learns current organism");
        System.out.println("loadPro \\\\loads player who can jump through all platforms but doesn't collect extra points");
        System.out.println("test \\\\Tests current organism on random game");
        System.out.println("exit \\\\exits");

        String readCommand = scanner.next();
        while(!readCommand.equals("exit")){
            if(readCommand.equals("loadPro")){
                po=createProPlayer();
                isPlayerOrganismLoaded=true;
            }else
            if(readCommand.equals("learn")){
                numberOfOrganisms = scanner.nextInt();
                numberOfGenerations = scanner.nextInt();
                ArrayList<PlayerOrganism> playerOrganisms = new ArrayList<>();
                if(!isPlayerOrganismLoaded)
                for(int i2=0;i2<numberOfOrganisms;i2++){
                    playerOrganisms.add(new PlayerOrganism(1,i,7, 10));
                }
                else
                    for(int i2=0;i2<numberOfOrganisms;i2++){
                        playerOrganisms.add(po);
                    }
                GeneticPlayerAlgorithm gpa = new GeneticPlayerAlgorithm(playerOrganisms,5,2,new WholeNeuronEnhancing(0.2), new WholeNeuronEnhancing((0.2)));
                for (int i2 = 0; i2 < numberOfGenerations; i2++) {
                    System.out.println(gpa.getGenerationNumber() + "   " + (gpa.getEfficiencyOfGeneration()));
                    gpa.createNewGeneration();

                }
                po=gpa.getBestOrganism();
                isPlayerOrganismLoaded=true;

            }else
            if(readCommand.equals("test")){
                   for (int i2 = 0;i2<100;i2++)po.playGame((new Random()).nextInt());
                System.out.println("średni wynik "+po.getEfficiency());
                System.out.println("w "+po.getPlayedGames()+" grach");

            }
            readCommand=scanner.next();
        }
    }

    /**
     *
     * @return playerOrganism which plays pretty good
     */
     public static PlayerOrganism createProPlayer(){
        try {
            Matrix2D[] idealMatrixes = new Matrix2D[2];
            idealMatrixes[0] = new Matrix2D(7, 10, 0);
            idealMatrixes[0].setValue(4, 0, -1);
            idealMatrixes[0].setValue(6, 0, 1);
            idealMatrixes[0].setValue(5, 1, 1);
            idealMatrixes[0].setValue(6, 1, -1);
            idealMatrixes[1] = new Matrix2D(10, 2, 0);
            idealMatrixes[1].setValue(0, 0, 1);
            idealMatrixes[1].setValue(1, 1, 1);
            int[] i=new int[3];
            i[0]=i[1]=i[2]=10;
            PlayerOrganism po = new PlayerOrganism(1, i, 7, idealMatrixes,10);

            return po;
        }

        catch (FlatMatrixException e){
            System.out.println("It shouldn't happen");
            e.printStackTrace();
        }
        return null;
    }
}

