package Genetic;

/**
 * Created by Albert on 19.01.2017.
 */
public interface IMutating<E> {
    E mutate(E e);
}
