package Genetic;

import Matrix.Matrix2D;

import java.util.Random;

/**
 * Created by Albert on 19.01.2017.
 */
public class NaiveGeneticEnhancing implements IMutating<PlayerOrganism>, ICrossingOver<PlayerOrganism>{
    /**
     * Mutates 10% of waages in transition matrixes (new values are random)
     * @param playerOrganism organism to be mutated
     * @return new, mutated PlayerOrganism
     */
    public PlayerOrganism mutate(PlayerOrganism playerOrganism) {
        Random random = new Random();
        double randomValue;
        Matrix2D[] mutatedTransitions = new Matrix2D[playerOrganism.getNumberOfNeuronColumns() + 1];
        for (int i = 0; i < playerOrganism.getTransitionMatrixes().length; i++) {
            mutatedTransitions[i] = playerOrganism.getTransitionMatrixes()[i].clone();
            for (int j = 0; j < mutatedTransitions[i].getLength(); j++) {
                for (int k = 0; k < mutatedTransitions[i].getHeight(); k++) {
                    randomValue = random.nextDouble();
                    if (randomValue < 0.1) {
                        mutatedTransitions[i].setValue(j, k, mutatedTransitions[i].getValue(j, k) + random.nextDouble() * 2d - 1d);
                        if (mutatedTransitions[i].getValue(j, k) < -1d)
                            mutatedTransitions[i].setValue(j, k, -1d);
                        if (mutatedTransitions[i].getValue(j, k) > 1)
                            mutatedTransitions[i].setValue(j, k, 1d);
                    }
                }
            }
        }
        return new PlayerOrganismBuilder().setFromPlayerOrganism(playerOrganism).setTransitionMatrixes(mutatedTransitions).createPlayerOrganism();
    }

    /**
     *
     * @param playerOrganism1 one of the parents
     * @param playerOrganism2 second of parents
     * @return new PlayerOrganism which has 50% wages from this and 50% from PlayerOrganism (from argument)
     */
    public PlayerOrganism crossOver(PlayerOrganism playerOrganism1, PlayerOrganism playerOrganism2) {
        Random random = new Random();
        double randomValue;
        Matrix2D[] mixedTransitions = new Matrix2D[playerOrganism1.getNumberOfNeuronColumns() + 1];
        for (int i = 0; i < playerOrganism1.getTransitionMatrixes().length; i++) {
            mixedTransitions[i] = playerOrganism1.getTransitionMatrixes()[i].clone();
            for (int j = 0; j < mixedTransitions[i].getLength(); j++)
                for (int k = 0; k < mixedTransitions[i].getHeight(); k++) {
                    randomValue = random.nextDouble();
                    mixedTransitions[i].setValue(j, k, randomValue < 0.5 ? playerOrganism1.getTransitionMatrixes()[i].getValue(j, k) : playerOrganism2.getTransitionMatrixes()[i].getValue(j, k));
                }

        }
        return new PlayerOrganismBuilder().setFromPlayerOrganism(playerOrganism1).setTransitionMatrixes(mixedTransitions).createPlayerOrganism();
    }


}
