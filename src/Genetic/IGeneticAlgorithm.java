package Genetic;

/**
 * Created by Albert Mosiałek on 18.12.2016.
 */
public interface IGeneticAlgorithm {
    void createPopulation(int population);
    void createNewGeneration();
    double getEfficiencyOfGeneration();
    int getGenerationNumber();

}
