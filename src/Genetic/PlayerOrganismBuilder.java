package Genetic;

import Matrix.Matrix2D;

/**
 * Created by Albert Mosiałek on 19.01.2017.
 */
public class PlayerOrganismBuilder {
    int numberOfNeuronColumns;
    int inputLength;
    int[] numberOfNeuronsInColumn;
    int maxPlatformLength;
    Matrix2D[] transitionMatrixes;
    public PlayerOrganismBuilder setNumberOfNeuronColumns(int newNumberOfNeuronColumns){
        numberOfNeuronColumns=newNumberOfNeuronColumns;
        return this;
    }

    public PlayerOrganismBuilder setInputLength(int inputLength) {
        this.inputLength = inputLength;
        return this;
    }

    public PlayerOrganismBuilder setMaxPlatformLength(int maxPlatformLength) {
        this.maxPlatformLength = maxPlatformLength;
        return this;
    }

    public PlayerOrganismBuilder setNumberOfNeuronsInColumn(int[] numberOfNeuronsInColumn) {
        this.numberOfNeuronsInColumn = numberOfNeuronsInColumn;
        return this;
    }

    public PlayerOrganismBuilder setTransitionMatrixes(Matrix2D[] transitionMatrixes) {
        this.transitionMatrixes = transitionMatrixes;
        return this;
    }

    public PlayerOrganismBuilder setFromPlayerOrganism(PlayerOrganism playerOrganism){
        numberOfNeuronColumns=playerOrganism.getNumberOfNeuronColumns();
        numberOfNeuronsInColumn=playerOrganism.getNumberOfNeuronsInColumn();
        maxPlatformLength=playerOrganism.getMaxPlatformLength();
        transitionMatrixes=playerOrganism.getTransitionMatrixes();
        inputLength=playerOrganism.getInputLength();
        return this;
    }

    public PlayerOrganism createPlayerOrganism(){
        return new PlayerOrganism(numberOfNeuronColumns,numberOfNeuronsInColumn,inputLength,transitionMatrixes,maxPlatformLength);
    }
}

