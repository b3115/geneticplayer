package Genetic;

/**
 * Created by Albert on 19.01.2017.
 */
public interface ICrossingOver<E> {
    E crossOver(E e1, E e2);
}
