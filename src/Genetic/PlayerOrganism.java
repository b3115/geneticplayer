package Genetic;

import Game.Game;
import Game.WrongPlatformInitParams;
import Matrix.Matrix2D;
import Matrix.WrongMatrixMultiplyArgumentsException;

import java.util.Random;

/**
 * Created by Albert Mosiałek on 18.12.2016.
 */
public class PlayerOrganism implements Comparable<PlayerOrganism> {
    private int numberOfNeuronColumns;
    private int inputLength;
    private int[] numberOfNeuronsInColumn;
    private int sumedScore=0;
    private int playedGames = 0;
    private int maxPlatformLength;
    private Game game;
    private Matrix2D[] transitionMatrixes;

    /**
     *
     * @param numberOfNeuronColumns number of hidden layers of neural network
     * @param numberOfNeuronsInColumn array with number of neurons in each layer
     * @param inputLength length of input from game
     * @param transitionMatrixes array of matrixes with wages in neural network (Note that matrixes must match in order to multiply them)
     * @param maxPatformLength
     */
    public PlayerOrganism(int numberOfNeuronColumns, int[] numberOfNeuronsInColumn, int inputLength, Matrix2D[] transitionMatrixes, int maxPatformLength) {
        this.numberOfNeuronColumns = numberOfNeuronColumns;
        this.numberOfNeuronsInColumn = numberOfNeuronsInColumn;
        this.transitionMatrixes = transitionMatrixes;
        this.inputLength = inputLength;
        this.maxPlatformLength=maxPatformLength;
    }

    /**
     *
     * @param numberOfNeuronColumns number of hidden layers of neural network
     * @param numberOfNeuronsInColumn array with number of neurons in each layer
     * @param inputLength length of input from game
     * @param maxPatformLength
     */
    public PlayerOrganism(int numberOfNeuronColumns, int[] numberOfNeuronsInColumn, int inputLength, int maxPatformLength ) {
        this.numberOfNeuronColumns = numberOfNeuronColumns;
        this.numberOfNeuronsInColumn = numberOfNeuronsInColumn;
        this.inputLength = inputLength;
        transitionMatrixes = new Matrix2D[numberOfNeuronColumns + 1];
        if(numberOfNeuronColumns==0){
            transitionMatrixes[0]= new Matrix2D(new double[inputLength][2]);
            transitionMatrixes[0].setRandomValues(-1,1);
        }
        else {
            transitionMatrixes[0] = new Matrix2D(new double[inputLength][numberOfNeuronsInColumn[0]]);
            transitionMatrixes[0].setRandomValues(-1, 1);
            for (int i = 1; i < numberOfNeuronColumns; i++) {
                transitionMatrixes[i] = new Matrix2D(new double[numberOfNeuronsInColumn[i - 1]][numberOfNeuronsInColumn[i]]);
                transitionMatrixes[i].setRandomValues(-1d, 1d);
            }
            transitionMatrixes[numberOfNeuronColumns] = new Matrix2D(new double[numberOfNeuronsInColumn[numberOfNeuronColumns - 1]][2]);
            transitionMatrixes[numberOfNeuronColumns].setRandomValues(-1, 1);
        }
        this.maxPlatformLength=maxPatformLength;
    }

    public int getInputLength() {
        return inputLength;
    }

    public int getNumberOfNeuronColumns() {
        return numberOfNeuronColumns;
    }

    public int getMaxPlatformLength() {
        return maxPlatformLength;
    }

    public int[] getNumberOfNeuronsInColumn() {
        return numberOfNeuronsInColumn;
    }

    public Matrix2D[] getTransitionMatrixes() {
        Matrix2D[] copiedMatrixes= new Matrix2D[transitionMatrixes.length];
        for(int i=0;i<transitionMatrixes.length;i++)
            copiedMatrixes[i]=transitionMatrixes[i].clone();
        return copiedMatrixes;
    }

    public int getPlayedGames(){
        return playedGames;
    }
    /**
     *
     * @param input gameState
     * @return output of neural network for given input. It can return -1, 0 or 1
     */
    private int getOutput(double[] input){

        double[][] d=new double[1][input.length];
        for(int i=0;i<input.length;i++)
            d[0][i]=input[i];
        Matrix2D m=new Matrix2D(d);
        for(int i=0;i<numberOfNeuronColumns+1;i++){
            try {
                m = transitionMatrixes[i].multiplyBy(m);
                for(int j=0;j<m.getHeight();j++){
                    m.setValue(0,j,activationFunction(m.getValue(0,j)));
                }

            }
            catch (WrongMatrixMultiplyArgumentsException e){
            }
        }
            double output1 = m.getValue(0,0);
            double output2 = m.getValue(0,1);
       if(output1>=0 && output2>=0)
                return 0;   //jump
            if(output1>=0 && output2<0)
                return -1;  //moce left
            return 1;       //move right
    }

    /**
     * Plays game once. It makes max 100 moves. Game is rendering platforms in random positions.
     * @return Score of game
     * @throws WrongPlatformInitParams
     */
    public int playGame() {
       return playGame(new Random().nextInt());
    }

    /**
     * Plays game once. It makes max 100 moves
     * @param seed seed for platform generator
     * @return Score of game
     * @throws WrongPlatformInitParams
     */
    public int playGame(long seed) {
        try {
            game = new Game(maxPlatformLength);
            game.setSeed(seed);
            int nextMove;
            int moveCounter = 0;
            nextMove = getOutput(game.getGameState());
            while (!game.isGameOver() && game.getPlatformNumber() < 100 && moveCounter<600) {
                moveCounter++;
                if (nextMove < 0) {
                    game.moveLeft();
                } else {
                    if (nextMove == 0) {
                        game.moveUp();
                    } else {
                        game.moveRight();
                    }
                }
                nextMove = getOutput(game.getGameState());
            }
            playedGames++;
            sumedScore += game.getScore();
            return game.getScore();
        }
        catch(WrongPlatformInitParams e){
            throw new InternalException("Shouldn't happen",e);
        }

    }

    /**
     *
     * @return average score of games played
     */
    public float getEfficiency(){
        float playedGamesDouble=this.playedGames;
        float sumedScoreDouble=this.sumedScore;
        return sumedScoreDouble/playedGamesDouble;
    }

    /**
     * Compares this with another organism by efficiency
     * @param o Organism to be compared
     * @return
     */
    @Override
    public int compareTo(PlayerOrganism o) {

        return Math.round(1000*(o.getEfficiency()-getEfficiency()));
    }

    /**
     * activation function for neurons (replaces d with value from -1 to 1 using tanh())
     * @param d
     * @return
     */
    private double activationFunction(double d){
        return (2.0/(1+Math.exp(-2*d))-1);
    }

    public String getTransitionMatrixesToString(){
        String s="";
        for(int i=0;i<transitionMatrixes.length;i++){
            s+=transitionMatrixes[i].toString();
            s+='\n';
        }
        return s;
    }

    public void setTransitionMatrixes(Matrix2D[] m){
        transitionMatrixes=m;
    }
}
