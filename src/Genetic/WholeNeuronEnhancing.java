package Genetic;

import Matrix.Matrix2D;

import java.util.Random;

/**
 * Created by Albert Mosiałek on 22.01.2017.
 */
public class WholeNeuronEnhancing implements IMutating<PlayerOrganism>, ICrossingOver<PlayerOrganism> {

    private double perentageOfMutatedNeurons;

    public WholeNeuronEnhancing(double perentageOfMutatedNeurons) {
        this.perentageOfMutatedNeurons=perentageOfMutatedNeurons;
    }

    @Override
    public PlayerOrganism mutate(PlayerOrganism playerOrganism) {
        Matrix2D[] mutatedTransitionMatrixes =  playerOrganism.getTransitionMatrixes();
        Random random = new Random();

        for(int i=0;i<mutatedTransitionMatrixes.length;i++) {

            for (int j = 0; j < mutatedTransitionMatrixes[i].getHeight(); j++) {

                if(random.nextDouble()<perentageOfMutatedNeurons){
                    mutatedTransitionMatrixes[i].setRowValuesRandom(j,-1,1);
                }

            }

        }

        return new PlayerOrganism(playerOrganism.getNumberOfNeuronColumns(),playerOrganism.getNumberOfNeuronsInColumn(),playerOrganism.getInputLength(),mutatedTransitionMatrixes,playerOrganism.getMaxPlatformLength());
    }


    @Override
    public PlayerOrganism crossOver(PlayerOrganism playerOrganism1, PlayerOrganism playerOrganism2) {
        Random random = new Random();
        Matrix2D[] mixedTransitionMatrixes=playerOrganism1.getTransitionMatrixes();
        for(int i=0;i<mixedTransitionMatrixes.length;i++) {
            for (int j = 0; j < mixedTransitionMatrixes[i].getHeight(); j++) {
                if(random.nextDouble()<0.5)
                    mixedTransitionMatrixes[i].setRowValues(j, playerOrganism2.getTransitionMatrixes()[i].getRowValues(j));
            }

        }
        return new PlayerOrganism(playerOrganism1.getNumberOfNeuronColumns(), playerOrganism1.getNumberOfNeuronsInColumn(), playerOrganism1.getInputLength(), mixedTransitionMatrixes, playerOrganism1.getMaxPlatformLength());
    }
}
