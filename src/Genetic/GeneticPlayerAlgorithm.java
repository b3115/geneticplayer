package Genetic;

import Game.WrongPlatformInitParams;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by Albert on 19.12.2016.
 */
public class GeneticPlayerAlgorithm implements IGeneticAlgorithm {

    int numberOfNeuronsColumns;
    int[] numberOfNeuronsInColumn;
    int inputLength;
    int generation=0;
    int numberOfBestOrganisms;
    int numberOfChildrenPerPair;
    int maxPlatformLength;
    long seed;
    ArrayList<PlayerOrganism> playerOrganisms;
    IMutating<PlayerOrganism> mutator;
    ICrossingOver<PlayerOrganism> crossOver;

    public GeneticPlayerAlgorithm(int numberOfNeuronsColumns, int[] numberOfNeuronsInColumn, int inputLength, int numberOfBestOrganisms, int numberOfChildrenPerPair, int maxPlatformLength, IMutating<PlayerOrganism> mutator, ICrossingOver crossOver)throws WrongPlatformInitParams {
        this.numberOfNeuronsColumns = numberOfNeuronsColumns;
        this.numberOfNeuronsInColumn = numberOfNeuronsInColumn;
        this.inputLength = inputLength;
        this.numberOfBestOrganisms = numberOfBestOrganisms;
        this.numberOfChildrenPerPair = numberOfChildrenPerPair;
        if(maxPlatformLength<=0)
            throw new WrongPlatformInitParams("Max platform length must be greater than 0");
        this.maxPlatformLength=maxPlatformLength;
        this.mutator=mutator;
        this.crossOver=crossOver;
        seed = new Random().nextInt();

    }

    public GeneticPlayerAlgorithm(ArrayList<PlayerOrganism> playerOrganisms, int numberOfBestOrganisms, int numberOfChildrenPerPair, IMutating<PlayerOrganism> mutator, ICrossingOver crossOver){
        this.playerOrganisms=playerOrganisms;
        this.numberOfBestOrganisms=numberOfBestOrganisms;
        this.numberOfChildrenPerPair=numberOfChildrenPerPair;
        this.mutator=mutator;
        this.crossOver=crossOver;
        this.inputLength=playerOrganisms.get(0).getInputLength();
        this.numberOfNeuronsColumns=playerOrganisms.get(0).getNumberOfNeuronColumns();
        this.numberOfNeuronsInColumn=playerOrganisms.get(0).getNumberOfNeuronsInColumn();
        this.maxPlatformLength=playerOrganisms.get(0).getMaxPlatformLength();
        seed = new Random().nextInt();
    }

    /**
     * creates ArrayList of PlayerOrganism
     * @param population - number of organisms in one generation
     */
    @Override
    public void createPopulation(int population) {
         playerOrganisms = new ArrayList<PlayerOrganism>();

        for(int i=0;i<population;i++){
            playerOrganisms.add(new PlayerOrganism(numberOfNeuronsColumns,numberOfNeuronsInColumn,inputLength, maxPlatformLength));
        }
        generation=1;

    }

    /**
     * takes best organisms, mixes them and creates new, better generation with exactly the same number of organisms
     */
    @Override
    public void createNewGeneration() {
        int numberOfOrganisms = playerOrganisms.size();
        Random random=new Random();
        ArrayList<PlayerOrganism> newGeneration=new ArrayList<PlayerOrganism>();
        for (PlayerOrganism organism:playerOrganisms) {
            for(int i=0;i<10;i++)
            {
                organism.getEfficiency();
            }
        }


        Collections.sort(playerOrganisms);
        for(int i=0;i<numberOfBestOrganisms;i++) {
            for(int j=i+1;j<numberOfBestOrganisms;j++){
                for(int k=0;k<numberOfChildrenPerPair;k++){
                    newGeneration.add(crossOver.crossOver(playerOrganisms.get(i),playerOrganisms.get(j)));
                }
            }
        }
        int numberOfGeneratedChildren=newGeneration.size();
        while(newGeneration.size()<numberOfOrganisms) {
            newGeneration.add(mutator.mutate(newGeneration.get(random.nextInt(numberOfGeneratedChildren - 1))));
        }
        playerOrganisms=newGeneration;

        generation++;

        }

    /**
     * invokes PlayGame() on each organism 10 times and sorts them by efficiency
     * @return efficiency of the best organism
     */
    @Override
    public double getEfficiencyOfGeneration() {
        for (PlayerOrganism p:playerOrganisms) {
                p.playGame(seed);
        }
        Collections.sort(playerOrganisms);
        return playerOrganisms.get(0).getEfficiency();
    }

    public PlayerOrganism getBestOrganism(){
        Collections.sort(playerOrganisms);
        return playerOrganisms.get(0);
    }

    @Override
    public int getGenerationNumber() {
        return generation;
    }

    /**
     * Sets seed of game to be learned
     * @param seed
     */
    public void setSeed(long seed) {
        this.seed = seed;
    }
}


