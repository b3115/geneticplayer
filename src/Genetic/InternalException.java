package Genetic;

/**
 * Created by Albert Mosiałek on 18.01.2017.
 */
public class InternalException extends RuntimeException{
    public InternalException(String msg, Throwable cause){
        super(msg,cause);
    }
}
