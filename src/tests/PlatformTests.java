package Tests;

import Game.Platform;
import Game.WrongPlatformInitParams;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Albert Mosiałek on 18.12.2016.
 */
public class PlatformTests {

    @Test
    public void TestPlatformGenerator(){
        try {
            Platform prevPlatform = new Platform(0, 9, 10);
            Platform currentPlatform = new Platform(prevPlatform.getLeftBound(), prevPlatform.getRightBound(), 10);
            for (int i = 0; i < 1000; i++) {

                Assert.assertTrue(currentPlatform.getLeftBound() <= currentPlatform.getRightBound());

                Assert.assertTrue(currentPlatform.getLeftBound() <= prevPlatform.getRightBound());

                Assert.assertTrue(currentPlatform.getRightBound() >= prevPlatform.getLeftBound());

                Assert.assertTrue(currentPlatform.getExtraPointPosition() >= currentPlatform.getLeftBound() && currentPlatform.getExtraPointPosition() <= currentPlatform.getRightBound());

                Assert.assertTrue(currentPlatform.isExtraPointAvaiable());

                currentPlatform.collectExtraPoint();

                Assert.assertFalse(currentPlatform.isExtraPointAvaiable());

                prevPlatform = currentPlatform;
                currentPlatform = new Platform(prevPlatform.getLeftBound(), prevPlatform.getRightBound(), 10);


            }
        }
        catch(WrongPlatformInitParams e){}
    }
}
