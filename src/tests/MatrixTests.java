package Tests;

import Matrix.FlatMatrixException;
import Matrix.Matrix2D;
import Matrix.WrongMatrixMultiplyArgumentsException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Albert Mosiałek on 18.12.2016.
 */
public class MatrixTests {

    Matrix2D m1;
    Matrix2D m2;
    Matrix2D m3;
    @Before
    public void prepare(){
        double[][] d1=new double[3][2];
        double[][] d2=new double[1][3];
        double[][] d3=new double[1][2];
        d1[0][0]=1d;
        d1[1][0]=2d;
        d1[2][0]=3d;
        d1[0][1]=4d;
        d1[1][1]=5d;
        d1[2][1]=6d;
        d2[0][0]=7d;
        d2[0][1]=8d;
        d2[0][2]=9d;
        d3[0][0]=50d;
        d3[0][1]=122d;

         m1=new Matrix2D(d1);
         m2=new Matrix2D(d2);
         m3=new Matrix2D(d3);

    }

    @Test
    public void matrix2DMultiplyTest(){
        try {

            Assert.assertTrue(Matrix2D.multiply(m1, m2).equals(m3));

        }
        catch (WrongMatrixMultiplyArgumentsException e){
            e.printStackTrace();
        }
    }

    @Test
    public void equalsTest(){
        Matrix2D mET, mET2;
        mET=new Matrix2D(new double[1][2]);
        mET2=new Matrix2D(new double[1][2]);
        mET.setValue(0,0,50d);
        mET.setValue(0,1,112d);
        mET2.setValue(0,0,50d);
        mET2.setValue(0,1,122d);
        Assert.assertFalse(mET.equals(mET2));
        Assert.assertFalse(mET2.equals(mET));
    }

    @Test
    public void MultiplyTest(){
        try{
            Matrix2D m = new Matrix2D(2,2,0);
            Matrix2D m2= new Matrix2D(1,2,10);
            Matrix2D m3 = new Matrix2D(1,2,10);
            m2.setValue(0,1,9);
            m3.setValue(0,1,9);
            m.setValue(0,0,1);
            m.setValue(1,1,1);
            Assert.assertTrue(m.multiplyBy(m2).equals(m3));
        }
        catch (FlatMatrixException e){}

        catch (WrongMatrixMultiplyArgumentsException e){}
    }

}
