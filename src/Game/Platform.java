package Game;

import java.util.Random;

/**
 * Created by Albert Mosiałek on 18.12.2016.
 */
public class Platform {
    private int maxLength;
    private int leftBound;
    private int rightBound;
    private boolean isExtraPointAvaiable;
    private int extraPointPosition;
    Random random;


    /**
     * Initializes this platform with following rules:
     *  - Right bound is greater or equal Left bound
     *  - Previous platform has at least one common field witk this one (you can always get here somehow)
     *  - Extra point position is always between Left and Right bound (also it can be on one of them)
     *  - Left bound is never lower than 0
     *  - Right bound is never greater than maxLength-1
     * @param leftPrev Left bound of previous platform
     * @param rightPrev Right bound of previous platform
     * @param maxLength
     */
    private void init(int leftPrev, int rightPrev, int maxLength) {
        this.maxLength=maxLength;
        leftBound=random.nextInt(maxLength);
        while (leftBound>rightPrev)
            leftBound=random.nextInt(maxLength);
        rightBound=random.nextInt(maxLength-leftBound+1)+leftBound;
        while (rightBound<leftPrev)
            rightBound=random.nextInt(maxLength-leftBound+1)+leftBound;
        isExtraPointAvaiable=true;
        extraPointPosition=random.nextInt(rightBound-leftBound+1)+leftBound;
    }

    /**
     * Creates new platform with init() function
     * @param leftPrev Left bound of previous platform
     * @param rightPrev Right bound of previous platform
     * @param maxLength
     * @throws WrongPlatformInitParams Thrown when maxLength <= 0
     */

    public Platform(int leftPrev, int rightPrev, int maxLength) throws WrongPlatformInitParams{
        if(rightPrev<leftPrev || maxLength<=0)
            throw new WrongPlatformInitParams();
        random=new Random();
        init(leftPrev,rightPrev,maxLength);

    }


    public Platform(int leftPrev, int rightPrev, int maxLength, Random random) throws WrongPlatformInitParams{
        if(rightPrev<leftPrev || maxLength<=0)
            throw new WrongPlatformInitParams();
        this.random=random;
        init(leftPrev,rightPrev,maxLength);
    }

    /**
     * Creates new platform which can be reached from the previous one
     * @param prevPlatform
     */
    public Platform(Platform prevPlatform, Random random) {
        this.random=random;
        init(prevPlatform.getLeftBound(),prevPlatform.getRightBound(),prevPlatform.getMaxLength());
    }

    /**
     * Creates new platform which can be reached from the previous one
     * @param prevPlatform
     */
    public Platform(Platform prevPlatform) {
        this.random=new Random();
        init(prevPlatform.getLeftBound(),prevPlatform.getRightBound(),prevPlatform.getMaxLength());
    }

    /**
     * Creates platform with given bounds Extra point params
     * @param maxLength
     * @param leftBound
     * @param rightBound
     * @param isExtraPointAvaiable
     * @param extraPointPosition
     * @throws WrongPlatformInitParams thrown when either Right bound<Left bound or maxLength<=0
     */
    public Platform(int maxLength, int leftBound, int rightBound, boolean isExtraPointAvaiable, int extraPointPosition) throws WrongPlatformInitParams{
        if(rightBound<leftBound || maxLength<=0)
            throw new WrongPlatformInitParams();
        this.maxLength = maxLength;
        this.leftBound = leftBound;
        this.rightBound = rightBound;
        this.isExtraPointAvaiable = isExtraPointAvaiable;
        this.extraPointPosition = extraPointPosition;
    }



    public int getLeftBound(){
        return leftBound;
    }

    public int getRightBound(){
        return rightBound;
    }

    public boolean isExtraPointAvaiable(){
        return isExtraPointAvaiable;
    }

    public int getExtraPointPosition(){
        return extraPointPosition;
    }

    /**
     * Sets isExtraPointAvaiable to False
     */
    public void collectExtraPoint(){
        isExtraPointAvaiable=false;
    }

    public int getMaxLength(){
        return maxLength;
    }
}
