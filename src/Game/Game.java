package Game;

import java.util.Random;

/**
 * Created by Albert Mosiałek on 18.12.2016.
 */
public class Game {
    int playerPosition;
    int maxPlatformLength;
    Integer score;
    Platform nextPlatform;
    Platform currentPlatform;
    private int platformNumber;
    private boolean isGameOver;
    private Random random;

    /**
     *
     * @param maxPlatformLength Indicates ho long can max platform length in game be
     * @throws WrongPlatformInitParams
     */
    public Game(int maxPlatformLength) throws WrongPlatformInitParams{
        this.maxPlatformLength = maxPlatformLength;
        try{
        currentPlatform = new Platform(maxPlatformLength, 0, 9, false, 0);
        }
        catch (WrongPlatformInitParams e){
            throw e;
        }
        random=new Random();
        nextPlatform = new Platform(currentPlatform, random);
        playerPosition = 5;
        score = 0;
        platformNumber=0;

    }

    /**
     * checks if player stands in place with extra point
     */
    private void checkForExtraPoint() {
        if (playerPosition == currentPlatform.getExtraPointPosition() && currentPlatform.isExtraPointAvaiable()) {
            score+=1;
            currentPlatform.collectExtraPoint();
        }
    }

    public int getPlatformNumber(){
        return platformNumber;
    }

    /**
     * decreases player position by 1
     * If player position is 0 nothing will change
     */
    public void moveLeft() {
        if (playerPosition != 0) {
            playerPosition--;
            if (playerPosition < currentPlatform.getLeftBound()) {
                isGameOver = true;
                return;
            }
            checkForExtraPoint();
        }

    }

    /**
     *
     * increases player position by 1
     * If player position is equal to maxPlatformLength-1 nothing will change
     */

    public void moveRight() {

        if (playerPosition != maxPlatformLength - 1) {
            playerPosition++;
            if (playerPosition > currentPlatform.getRightBound()) {
                isGameOver = true;
                return;
            }
            checkForExtraPoint();
        }

    }

    /**
     * if player is below next platform, game moves player to next platform and generates new one
     */
    public void moveUp() {
        if (playerPosition >= nextPlatform.getLeftBound() && playerPosition <= nextPlatform.getRightBound()) {
            score++;
            platformNumber++;
            currentPlatform = nextPlatform;
            nextPlatform = new Platform(currentPlatform, random);
            checkForExtraPoint();
        }

    }

    /**
     *
     * @return double[] with info about platforms bound (current and next), is extra point avaiable, it's position and player position
     */
    public double[] getGameState() {
        double[] i = new double[7];
        i[0] = currentPlatform.getLeftBound();
        i[1] = currentPlatform.getRightBound();
        i[2] = currentPlatform.getExtraPointPosition();
        i[3] = currentPlatform.isExtraPointAvaiable() ? 1 : 0;
        i[4] = nextPlatform.getLeftBound();
        i[5] = nextPlatform.getRightBound();
        i[6] = playerPosition;
        return i;

    }

    /**
     * checks if game is already over
     */
    public boolean isGameOver() {
        return isGameOver;
    }

    public int getScore() {
        return score;
    }

    /**
    *  @return GameState in String which allows to print it
    */
    public String getGameStateToString() {
        StringBuilder sb = new StringBuilder();
        double[] gameState = getGameState();
        for (int i = 0; i <= 6; i++) {
            sb.append(gameState[i]);
            sb.append(", ");
        }
        return sb.toString();
    }

    public void setSeed(long seed){
        random=new Random(seed);
        nextPlatform=new Platform(currentPlatform,random);
    }
}