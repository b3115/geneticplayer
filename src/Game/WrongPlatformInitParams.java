package Game;

/**
 * Created by Albert Mosiałek on 18.01.2017.
 */
public class WrongPlatformInitParams extends Exception  {
    public WrongPlatformInitParams(String message){
        super(message);
    }

    public WrongPlatformInitParams(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongPlatformInitParams() {
    }
}
